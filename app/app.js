require.config ({
    baseUrl: 'app',
    paths: {
        /* lib */
        backbone:           '../../node_modules/backbone/backbone',
        jquery:             '../../node_modules/jquery/dist/jquery',
        underscore:         '../../node_modules/underscore/underscore',
        vue:                '../../node_modules/vue/dist/vue',

        /* m */
        baseModel:          'js/models/baseModel',
        mainModel:          'js/models/mainModel',
        genderModel:        'js/models/genderModel',

        /* v */
        baseView:               'js/views/baseView',
        baseCollectionView:     'js/views/baseCollectionView',
        collectionView:         'js/views/collectionView',
        genderCollectionView:   'js/views/genderCollectionView',
        genderView:             'js/views/genderView',
        mainView:               'js/views/mainView',
        optionsView:            'js/views/optionsView',

        /* c */
        baseCollection:     'js/collections/baseCollection',
        collection:         'js/collections/collection',
        genderCollection:   'js/collections/genderCollection',

        /* r */
        baseRoute:          'js/routes/baseRoute',
        route:              'js/routes/route'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        }
    }
});

define(['collectionView', 'collection', 'route'], 
    function(collectionView, collection, route) {
        new route;
        Backbone.history.start();

        new collectionView ({
            collection: new collection
        });
    }); 
