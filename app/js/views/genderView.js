define(['baseView'], 
    function(BaseView) {
        return genderView = BaseView.extend({
            initialize: function() {
                this.render();
            },

            el: "#content", 

            template: _.template($("#gender-template").html()),

            render: function() {
                return this.$el.html(this.template(this.model.toJSON()));
            }
        }); 
    });
