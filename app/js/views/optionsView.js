define(['baseView', 'genderView', 'genderCollection', 'genderModel'], 
    function(BaseView, genderView, genderCollection, genderModel) {
        return optionsView = BaseView.extend({
            initialize: function() {
                this.fetch();
            },

            events: {
               'submit': 'filter'
            },

            filter: function(e) {
                e.preventDefault();

                function getAge() {
                    const age = $("input[name='age']:checked").val();
                    return age;
                }

                function getSex() {
                    const gender = $("input[name='gender']:checked").val();
                    return gender;
                }

                switch (getAge()) {
                    case 'any':
                        var min_age = 18, max_age = 99;
                        break;
                    case 'twenties':
                        var min_age = 18, max_age = 29;
                        break;
                    case 'thirties':
                        var min_age = 30, max_age = 39;
                        break;
                    case 'forties':
                        var min_age = 40, max_age = 49;
                        break;
                    case 'fifties':
                        var min_age = 50, max_age = 59;
                        break;
                    case 'sixties':
                        var min_age = 60, max_age = 69;
                        break;
                    default:
                        console.log("sorry...");
                }

                const generic_set = _.filter(this.model.attributes.results, function (json) {
                    if ((((json.dob.age >= min_age) && (json.dob.age <= max_age)) && (json.gender == getSex())) || ((json.dob.age >= min_age) && (json.dob.age <= max_age)) && (getSex() == "both")) {
                        return json;
                    }
                });

                const gc = new genderCollection([new genderModel(generic_set)]);

                gc.each(function(item) {
                    new genderView({
                        model: item
                    });
                });

                let nationality = _.filter(this.model.attributes.results, function(json) {
                    return json;
                });
            },
            template: _.template($("#options-template").html())
        }); 
    });
