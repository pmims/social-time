define(['baseCollectionView'], 
    function(BaseCollectionView) {
        return collectionView = BaseCollectionView.extend({
            fetch: function() {
                this.listenTo(this.collection, "reset", this.render);
                this.collection.fetch({reset: true});
            },

            render: function() {
                this.collection.each(function(item) {
                    this.renderModel(item);
                }, this);
            },

            renderModel: function(item) {
                new optionsView ({
                    el: "#options",
                    model: item
                });
                new mainView ({
                    el: "#content",
                    model: item
                });
            }
        });
    });
