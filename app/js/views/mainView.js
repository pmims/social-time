define(['baseView'], 
    function(BaseView) {
        return mainView = BaseView.extend({
            initialize: function() {
                this.fetch();
            },

            template: _.template($("#main-template").html())
        }); 
    });
