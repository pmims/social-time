define(['jquery', 'underscore', 'backbone'], 
    function($, _, Backbone) {
        return baseView = Backbone.View.extend({
            initialize: function() {
                console.log("baseView");
                console.log(this.model.attributes);
                this.render();
            },

            fetch: function() {
                this.listenTo(this.model, 'change', this.render);
                this.model.fetch();
            },

            render: function() {
                return this.$el.html(this.template(this.model.toJSON()));
            }
        }); 
    });
