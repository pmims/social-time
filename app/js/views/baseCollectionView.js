define(['baseView', 'optionsView', 'mainView', 'genderView'], 
    function(baseView, optionsView, mainView, genderView) {
        return collectionViewBase = baseView.extend({
            initialize: function() {
                console.log("baseView");
                console.log("collectionViewBase");
                this.fetch();
                this.render();
            }
        });
    });
