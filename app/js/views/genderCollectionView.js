define(['baseCollectionView'], 
    function(BaseCollectionView) {
        return genderCollectionView = Backbone.Collection.extend({
            initialize: function() {
                console.log("GENDERCOLLECTIONVIEW INIT.");
                console.log(this.collection);
            },

            render: function() {
                this.collection.each(function(item) {
                    console.log(item);
                });
            },
        });
    });
