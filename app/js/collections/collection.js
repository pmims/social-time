define(['baseCollection', 'mainModel'], 
    function(BaseCollection, mainModel) {
        return collection = BaseCollection.extend({
            defaults: {
                unit: '500'
            },

            model: mainModel,

            url: function(unit) {
                return "https://randomuser.me/api/?results=500"
                //return "https://randomuser.me/api/?results=" + this.get('unit'); 
            }
        });
    });
