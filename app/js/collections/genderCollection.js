define(['baseCollection', 'genderModel'],
    function (BaseCollection, genderModel) {
        return collection = BaseCollection.extend({
            model: genderModel
        });
    });