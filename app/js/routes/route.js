define(['baseRoute'], 
    function(BaseRoute) {
        return routes = BaseRoute.extend({
            routes: {
                'init'                  : '_init', 
                'changeUnit/:unit'      : '_changeUnit',
                'options'               : '_showView',
                'daily/:zip'            : '_daily',
                'hourly/:zip'           : '_hourly',
                'main/:zip'             : '_main',
                'map'                   : '_map',
                'search'                : '_search',
                ''                      : '_start',
                'uv'                    : '_uv',
                '*default'              : '_default'
            }, 
            _init: function() {
                console.log("Init Route");
            },

            showView: function() {
                new optionsView({
                    el: "#options"
                });
            },

            _default: function() {
                console.log("Default Route");
            }
        });
    });
