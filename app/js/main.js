(function() {
    var element = document.getElementById("preferences");
    var options = document.getElementById("options");

    element.addEventListener("click", function(e) {
        e.preventDefault();
        if(options.style.display === "none") {
            options.style.display = "block";
        } else {
            options.style.display = "none";
        }
    });
})();
